new Vue({
    el:'#vue-app',
    data:{
        name:'Shuan',
        job:"Ninja",
        website:"http://bufferedky.com",
        webtag:'<a href="http://bufferedky.com">bitlab</a>'
    },
    
    methods:{
        greet:function(time){
           
            return 'Good ' + time + ', ' + this.name;
        }
    }
    
});
